* Project sử dụng Reactjs + Babel 6 + Webpack

* Hướng dẫn sử dụng :
    - mở cmd và nhập : 
    cd testdemo
    node_modules\.bin\webpack -d

    - mở file testdemo\src\app\index.html



* Tài Khoản Đăng Nhập:
   {id :"leduydat", password : "123", role : "user"},       (Chỉ có thể xem)
   {id :"dieuthuyen", password : "123", role : "manager"},  (Có thể xem, thêm, xóa, sửa)
   {id :"dongtrac", password : "123", role : "employee"},   (chỉ được xem và thêm)

    




 1) Kể tên những thư viện CSS và JS bạn đã dùng, kể cả trong project trên, chức năng và ưu điểm của      các thư viện đó:
* Thư viện ReactJS:
    - Chức năng : React là 1 thư viện của Facebook giúp render ra phần view.
    - Ưu điểm : React hỗ trợ việc xây dựng những thành phần (components) UI có tính tương tác cao, có   trạng thái và có thể sử dụng lại được.
* Axios :
    - Chức năng : hỗ trợ cho việc xây dựng các ứng dụng API
    - Ưu điểm : dễ sử dụng, dễ bắt lỗi hơn fetch
* Webpack : (sử dụng ở project trên)
    - Chức năng : Webpack là công cụ giúp gói gọn toàn bộ file js, css(bao gồm cả scss,sass,..)
    - Ưu điểm : Webpack hiện đang là module loader được sử dụng rộng rãi nhất hiện nay với cộng động support to lớn và những chức năng vô cùng mạnh mẽ
* Redux: 
    - Chức Năng :  Giúp bạn viết các ứng dụng hoạt động một cách nhất quán, chạy trong các môi trường   khác nhau (client, server, and native)
    - Ưu điểm : Khi dự án phình to, component nhiều dẫn tới việc khó quản lí. Việc Component con thay   đổi 1 state trong component cha phải props xuống nhiều bước -> phức tạp, khó quản lí -> lúc này   ta cần tới redux.

2)Trình bày về xử lý bất đồng bộ trong JS và bạn đã dùng chỗ nào trong project trên;
    - Javascript là một ngôn ngữ không đồng bộ,nghĩa là khi code được chạy tuần tự từ trên xuống dưới,đoạn code trên chưa xử lí xong thì đoạn phía dưới đã bắt đầu chạy.Vì vậy ta phải  xử lí bất đồng bộ trong javascript.

    - Ở project trên có sử dụng xử lí bất đồng bộ cho việc thêm, xóa, sửa 1 product.

3) ReactJS, Jquery, Angular khác nhau như thế nào? 
    - jQuery là thư viện để thao tác với DOM 
    - AngularJs là MVC hay MVVM framework
    - ReactJs là thư viện để xây dựng giao diện, nó chỉ là phần V trong MVC.

4) Trình bày về Floats và cách chúng hoạt động;
    - Float là một kỹ thuật mà chúng ta thiết lập phần tử HTML trôi dạt về một hướng nào đó để lấp - đầy các khoảng trống nếu có thể trong một khung chứa.Đặc biệt Khi một phần tử float, thì các phần tử kề cạnh nó sẽ trôi dạt theo để lấp đầy khoảng trống đó nếu có thể.

    -Khi áp dụng float chúng ta có bốn phần chính cần quan tâm:
        + Khung chứa
        + Phần tử trôi dạt
        + Các phần tử bị trôi dạt kéo theo
        + Kích thước của phần tử trôi dạt

    
5)Trình bày về z-index và làm thế nào để nội dung stack với nhau được định hình;
    - Mỗi element trên trang web được hiển thị ngang và dọc theo 2 trục x và y, hiển thị thứ tự chồng lấn theo trục z.
    - Khi các block thuộc cùng 1 stacking context thì thứ tự trên dưới luôn được đảm bảo theo quy tắc: element nào có z-index cao hơn sẽ hiện lên trên. Nếu chưa có một element nào được set z-index thì thứ tự sẽ phụ thuộc vào thứ tự xuất hiện từ trước ra sau của DOM tree.

6)  Giải thích về CSS sprites và làm thế nào để bạn thực hiện chúng trên một trang web.
    
    - CSS sprites là một kỹ thuật giúp tối ưu việc tải trang bằng cách giảm thiểu số lượng HTTP requests và dung lượng của các tài nguyên (file ảnh, icon...) cần thiết cho website.

    - Trước tiên, để sử dụng Css Sprites thì cần phải chuẩn bị tập tin hình ảnh đã ghép (tập tin sprite), dùng background-position trỏ tới vị trí của hình ảnh trên tập ảnh đã ghép.







