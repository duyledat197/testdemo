import React, { Component } from 'react'
import  './Login.css'
import {database} from './../interface/firebase'
// var account = [
//     {id :"leduydat", password : "123", role : "user"},
//     {id :"dieuthuyen", password : "123", role : "manager"},
//     {id :"dongtrac", password : "123", role : "employee"},
//     {id :"lubo", password : "123", role : "user"},
// ]

class Login extends React.Component {
    constructor(props){
        super(props);
    }
    async handleClickLogin(e){
        // this.props.setIsLogin(e);
        let idUser = document.getElementById('login-id').value;
        let passwordUser = document.getElementById('login-password').value;
        let finding = false;
        // let finding = account.find(function(element) {
        //     return element.id === idUser && element.password === passwordUser;
        // });
        // await firestore.collection('users').get().then((querySnapshot) => {
        //     querySnapshot.forEach((doc) => {
        //         let user = doc.data();
        //         if(idUser == user.id && passwordUser == user.password){
        //             this.props.setIsLogin(e,user.role);
        //             finding = true;
        //         }    
        //     });
        // });
        let role;
        await database.ref('users').once('value')
        .then((dataSnapshot) => {
          // handle read data.
          dataSnapshot.forEach(snapshot => {
              let snapshotVal = snapshot.val();
              console.log(snapshotVal);
              
              if(idUser == snapshotVal.id && passwordUser == snapshotVal.password){
                // this.props.setIsLogin(e,snapshotVal.role);
                role = snapshotVal.role;
                finding = true;
              }
          })
          
        });
        
        if(finding == false) alert("Tài khoản hoặc Mật khẩu của bạn không chính xác !!!");
         else this.props.setIsLogin(e,role);
    }
    render () {
        return (
            <div className="body">
                <div className="loginBox">
                    <div className="loginBox-title"> Đăng Nhập</div>
                    <div className="loginBox-buff">
                    <div className="loginBox-text"> Tài Khoản  </div>
                    <input className="loginBox-input" placeholder="Nhập tài khoản" id="login-id"/>
                    <div className="loginBox-text"> Mật Khẩu  </div>
                    <input className="loginBox-input" type="password" placeholder="Nhập mật khẩu" id="login-password"/>
                    <button id="loginBox-button" onClick = { (e) => this.handleClickLogin(e)}> Đăng Nhập </button>
                    
                    </div>
                </div>
            </div>
        )
    }
}

export default Login