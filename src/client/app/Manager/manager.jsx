import React, { Component } from 'react'
import './manager.css'
import { database } from './../interface/firebase'
var statusArray = ["Hiện", "Ẩn"];
var uid = require('uid');
var tempt = true;
// var product = [
//     { id: "SAG111", title : "Giày nhỏ", content : "giày cá tính", imgURL :"", status : 0},
//     { id: "SA112", title : "Giày nhỏ", content : "giày dành cho nữ", imgURL :"", status : 1},
//     { id: "SAW113", title : "Giày nhỏ", content : "giày nhỏ", imgURL :"", status : 1},
//     { id: "SA114", title : "Giày nhỏ", content : "giày lớn", imgURL :"", status : 0},
//     { id: "QA115", title : "Giày nhỏ", content : "giày đeo", imgURL :"", status : 0},
//     { id: "QQ115", title : "Giày nhỏ", content : "giày đeo", imgURL :"", status : 0},
//     { id: "DAS115", title : "Giày nhỏ", content : "giày đeo", imgURL :"", status : 0},
// ];
class PropsProduct extends React.Component{
    constructor(props){
        super(props);
        // this.handleClickEditSave = this.handleClickEditSave.bind(this);
        this.handleClickDelete = this.handleClickDelete.bind(this);
    }
    componentWillMount(){
        this.setState({ isEdit : false,
            disabled : true
        });
        // console.log("will mount");
        // console.log(this.props.role);
        
        
        
    }
    componentDidMount(){
        document.getElementById(this.props.id + "id").value = this.props.id;
        document.getElementById(this.props.id + "title").value = this.props.title;
        document.getElementById(this.props.id + "content").value = this.props.content;
        document.getElementById(this.props.id + "status").value = this.props.status;
        // document.getElementById(this.props.id + "imgURL").value = this.props.imgURL;
        // console.log("didmount");
    }
    async handleClickEditSave(e){
        var id = document.getElementById(this.props.id + "id").value;
        var title = document.getElementById(this.props.id + "title").value;
        var content = document.getElementById(this.props.id + "content").value;
        var status = document.getElementById(this.props.id + "status").value; 
        var imgURL = document.getElementById(this.props.id + "imgURL").value; 
        // console.log("editsave");
        let check = false;
        await database.ref('products').once('value').then(dataSnapshot =>{
            dataSnapshot.forEach(snapshot =>{
                if(snapshot.val().id == id && snapshot.val().url != this.props.url) {
                    check = true;

                }
            })
        })
        if(check == false){
             
            this.setState({ isEdit : false,disabled : true});
            await database.ref('products/' + this.props.url).update({
                    id : id,
                    title : title,  
                    content : content,
                    status : status,
                    imgURL : imgURL
            })
            // this.props.saveProductEdit(data);
            // console.log(this.props.id + "id");
            
            // document.getElementById(this.props.id + "id").disabled = true;
            // document.getElementById(this.props.id + "title").disabled = true;
            // document.getElementById(this.props.id + "content").disabled = true;
            // document.getElementById(this.props.id + "status").disabled = true;  
        }
        else alert("Mã trùng");
    }
    handleClickEdit(e){
        this.setState({isEdit : true,
            disabled : false
        });
        console.log("edit");
        
        // document.getElementById(this.props.id + "id").disabled = false;
        // document.getElementById(this.props.id + "title").disabled = false;
        // document.getElementById(this.props.id + "content").disabled = false;
        // document.getElementById(this.props.id + "status").disabled = false;
        // document.getElementById(this.props.id + "id").value = "";
    }
    handleClickDelete(e){
        // let id = document.getElementById(this.props.id + "id").value;
        if (confirm('Bạn muốn xóa nó chứ ?')) {
            this.props.deleteProduct(e,this.props.url);
        } else {
            // Do nothing!
        }
        
    }
    handleClickCancel(e){
        this.setState({ isEdit : false,
            disabled : true
        });
        // document.getElementById(this.props.id + "id").disabled = true;
        // document.getElementById(this.props.id + "title").disabled = true;
        // document.getElementById(this.props.id + "content").disabled = true;
        // document.getElementById(this.props.id + "status").disabled = true;

        document.getElementById(this.props.id + "id").value = this.props.id;
        document.getElementById(this.props.id + "title").value = this.props.title;
        document.getElementById(this.props.id + "content").value = this.props.content;
        document.getElementById(this.props.id + "status").value = this.props.status;
        document.getElementById(this.props.id + "imgURL").value = this.props.imgURL;
    }
    // componentDidMount(){

    // }
    render(){
        if(this.props.id != null)
        return (
            <tr>          
                    <td> <input  className={ this.state.isEdit == true ? "manager-form-input" : "manager-table-input"} id={this.props.id + "id"}  disabled = { this.state.disabled}></input> </td>
                    <td> <input  className={ this.state.isEdit == true ? "manager-form-input" : "manager-table-input"} id={this.props.id + "title"} disabled = { this.state.disabled}></input> </td>
                    <td> <input  className={ this.state.isEdit == true ? "manager-form-input" : "manager-table-input"} id={this.props.id + "content"} disabled = { this.state.disabled}></input> </td>
                    <td> 
                         <select className="manager-select" id={this.props.id + "status"} disabled = { this.state.disabled} >
                                    <option value="0"> Ẩn </option>
                                    <option value="1"> Hiện </option>
                         </select> 
                    </td>
                    <td>
                      <input type="file" id={this.props.id + "imgURL"} disabled = { this.state.disabled}/>
                    </td>
                    <td >
                        
                        <button className={this.props.role === 'manager' ? "product-button" : "product-button-block"} onClick={ this.state.isEdit == false ? (e) => this.handleClickEdit(e) : (e) => this.handleClickEditSave(e) } disabled = { this.props.role != "manager" ? true : false}> {this.state.isEdit == false ? "Sửa" : "Save"} </button>
                        <button className={this.props.role === 'manager' ? "product-button" : "product-button-block"} onClick={ this.state.isEdit == false ? (e) => this.handleClickDelete(e) : (e) => this.handleClickCancel(e) } disabled = { this.props.role != "manager" ? true : false}> {this.state.isEdit == false ? "Xóa" : "Hủy"} </button>
                    </td>
        </tr>
        )
    }
}
class Manager extends React.Component {
    constructor(props){
        super(props);
        this.handleChangeStatus = this.handleChangeStatus.bind(this);
        this.saveProduct = this.saveProduct.bind(this);
        this.deleteProduct = this.deleteProduct.bind(this);
       
        // this.saveProductEdit = this.saveProductEdit.bind(this);
    }
    componentWillMount(){
        this.setState({ 
            status : 1,
            product : []
        });
    }
    async componentDidMount(){
         
        // await database.ref('products').once('value').then(dataSnapshot => {
        //     dataSnapshot.forEach(snapshot =>{
        //         product.push(snapshot.val());
        //     })
        //  })
        //  await this.setState({product : product});
            database.ref('products').on('value',dataSnapshot => {
            // console.log(dataSnapshot.val());
            var product = [];
             dataSnapshot.forEach(snapshot => {
                // console.log(snapshot.val());
                
                if(snapshot.exists){
                    product.push(snapshot.val());
                }
            });
            this.setState({product : product});
        })
        
    }

    async getProduct(){
        var product = [];
        await database.ref('products').once('value').then(dataSnapshot => {
            dataSnapshot.forEach(snapshot => {
                if(snapshot.exists) product.push(snapshot.val());
            })
        })
        return product;
    }

   async handleChangeStatus(e){
        var product = await this.getProduct();
        if(this.state.status == 0){
            this.setState({status : 1 - this.state.status,
               product : [...product]
            })
        }
        else {

            let tempt = product.filter((e) => {
                return e.status == this.state.status
            })
            this.setState({ status : 1 - this.state.status,
            product : [...tempt]
            });
        }
    }
    async saveProduct(e){
        let id = document.getElementById('id').value;
        var finding = false;
        await database.ref('products').once('value').then(dataSnapshot => {
                
                 dataSnapshot.forEach(snapshot =>{
                    if(snapshot.val().id == id) {
                        finding = true;
                        console.log("co vao" + snapshot.val());        
                    }
                })               
        })
        if(finding == false){
            let url = uid(10);
            let title = document.getElementById('title').value;
            let content = document.getElementById('content').value;
            let statusProduct = parseInt(document.getElementById('status').value);
            let imgURL = document.getElementById('imgURL').value;
            console.log(document.getElementById('imgURL').value);
            
            var data = {
                id : id,
                title,
                content,
                status : statusProduct,
                imgURL
            }
            database.ref('products/' + url).set({
                url,
                id,
                title,
                content,
                status : statusProduct,
                imgURL
            })
            
        }
        else alert("Mã trùng");
        console.log(this.state.product[this.state.product.length - 1]);
        
    }
    resetInput(e){
        document.getElementById('id').value = '';
        document.getElementById('title').value = '';
        document.getElementById('content').value = '';
    }

    async deleteProduct(e,url){
        await database.ref('products/' + url).remove().then(function() {
            console.log("Remove succeeded.")
          })
          .catch(function(error) {
            console.log("Remove failed: " + error.message)
          });
    }
    async searchProduct(e){
        let search = document.getElementById('search').value;
        let product = await this.getProduct();
        if(search == ""){
            
            this.setState({
                product : product
            })
        }
        else {
            let tempt = this.state.product.filter((e) => {
                return e.id.match(search);
            })
            this.setState({ product : tempt}); 
        }
    }
    LogOut(e){
        this.props.setIsNotLogin(e);
    }
    render () {
        // console.log(this.state.product);
        
        return (
            <div className="manager-container">
                <div className={ this.props.role == 'user' ? "blockDiv" : "manager-save-reset-panel"}>
                    <button className="manager-button" onClick={ (e) => this.resetInput(e)}> Reset </button>
                    <button className="manager-button" onClick={ (e) => this.saveProduct(e) } > Save </button>
                    
                </div>
                <div className={ this.props.role == 'user' ? "blockDiv" : "manager-form-panel"}>
                    <div className="manager-from-top"> 
                        ID : &ensp;
                        <input className="manager-form-input" placeholder="Nhập ID" id="id"/>
                         &ensp; Tiêu Đề : &ensp;
                        <input className="manager-form-input" placeholder="Nhập Tiêu Đề" id="title"/>
                        &ensp; Trạng thái : &ensp;
                        <select id="status" className="manager-select">
                        <option value="0"> Ẩn </option>
                        <option value="1"> Hiện </option>
                        </select>
                    </div>
                    <div className="manager-from-bottom" >
                        Nội Dung : &ensp;<textarea className="manager-form-input" placeholder="Nhập Nội Dung" id="content"></textarea>
                        &ensp; Chọn hình ảnh : &ensp; <input type="file" id="imgURL"/>
                    </div>
                </div>
                <div className="manager-table-panel">
                <div className="manager-search">
                        <input className="manager-form-input" placeholder="Nhập tìm kiếm" id="search"/>
                        <button className="manager-button" onClick={ (e) => this.searchProduct(e)} > Tìm Kiếm </button>
                        <button className="manager-button" onClick={ (e) => this.handleChangeStatus(e)}>  Trạng Thái { statusArray[this.state.status] } </button>
                        <button className="manager-button" onClick={ (e) => this.LogOut(e)} > Đăng Xuất </button>
                </div>
                <table id="manager-table">
                    <tr>
                        <th> ID </th>
                        <th> Tiêu Đề  </th>
                        <th> Nội Dung </th>
                        <th> Trạng thái </th>
                        <th> Hình ảnh </th>
                        <th> Action </th>
                    </tr>
                    { this.state.product.map((e) => (
                        <PropsProduct {...e}  deleteProduct= { this.deleteProduct}   key={ e.url} role ={this.props.role}/>
                    ))}
                </table>
                {/* <input className="manager-table-input" value= "hello"disabled/> */}
                </div>
            </div>
        )
    }
}

export default Manager