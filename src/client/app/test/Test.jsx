import React, { Component } from 'react'
import './test.css'
var product = [
    { id: "SAG111", title : "Giày nhỏ", content : "giày cá tính", imgURL :"https://www.w3schools.com/howto/img_avatar.png", status : 0},
    { id: "SA112", title : "Giày nhỏ", content : "giày dành cho nữ", imgURL :"https://www.w3schools.com/howto/img_avatar.png", status : 1},
    { id: "SAW113", title : "Giày nhỏ", content : "giày nhỏ", imgURL :"https://www.w3schools.com/howto/img_avatar.png", status : 1},
    { id: "SA114", title : "Giày nhỏ", content : "giày lớn", imgURL :"https://www.w3schools.com/howto/img_avatar.png", status : 0},
    { id: "QA115", title : "Giày nhỏ", content : "giày đeo", imgURL :"https://www.w3schools.com/howto/img_avatar.png", status : 0},
    { id: "QQ115", title : "Giày nhỏ", content : "giày đeo", imgURL :"https://www.w3schools.com/howto/img_avatar.png", status : 0},
    { id: "DAS115", title : "Giày nhỏ", content : "giày đeo", imgURL :"https://www.w3schools.com/howto/img_avatar.png", status : 0},
];
class PropsList extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div className="manager-list-container">
                <div className="manager-list-img">
                    <img src={ this.props.imgURL } width="100%"/>
                </div>
                <div className="manager-list-content">
                    <input  className="manager-list-input" value={"ID : " + this.props.id} disabled/>
                    <input className="manager-list-input" value={"Tiêu Đề : " + this.props.title} disabled/>
                    <textarea className="manager-list-textarea" value={"Nội Dung : " + this.props.content} disabled/>
                </div>
                <div className="manager-list-button">
                    <button> Save </button>
                    <button> Xóa </button>
                </div>
            </div>
        )
    }
}
class Test extends React.Component {
    constructor(props){
        super(props);
    }
    render () {
        return (
            <div className="manager-list-panel">
                { product.map(
                    (e) => {
                      return  <PropsList {...e} key={ e.id}/>
                    }
                )}
            </div>
        )
    }
}

export default Test