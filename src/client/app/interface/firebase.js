import firebase from 'firebase/app'
import 'firebase/database'
import 'firebase/auth'
var config = {
    apiKey: "AIzaSyDh5bnDkmbRJ-W0dbb3vA32JrjgVWNb6-E",
    authDomain: "intern-ca958.firebaseapp.com",
    databaseURL: "https://intern-ca958.firebaseio.com",
    projectId: "intern-ca958",
    storageBucket: "intern-ca958.appspot.com",
    messagingSenderId: "897652325958"
  };
export default firebase.initializeApp(config);
export const database = firebase.database();