import React from 'react';
import {render} from 'react-dom';

import Login from './Login/Login.jsx'
import Manager from './Manager/manager.jsx'
import Test from './test/Test.jsx'
import firebase from './interface/firebase'
// function writeUserData(userId, password,role) {
//     firebase.database().ref('users/' + userId).set({
//       id : userId,
//       password: password,
//       role : role
//     });
//   }
class App extends React.Component {
    constructor(props){
        super(props);
        this.setIsLogin = this.setIsLogin.bind(this);
        this.setIsNotLogin = this.setIsNotLogin.bind(this);
    }


    componentWillMount(){
        this.setState({ isLogin : false,
            role : "user"
        });
    }   
    componentDidMount(){
        // writeUserData('dieuthuyen','123','manager');
        // writeUserData('dongtrac','123','employee');
    } 
    setIsLogin(e, rolle){
        
        this.setState({ isLogin : true,
                        role : rolle
        });
    }
    setIsNotLogin(e){
        this.setState({
            isLogin : false
        })
    }
    render () {
        if(! this.state.isLogin)  return <Login setIsLogin = { this.setIsLogin }/>
        else return <Manager role = {this.state.role} setIsNotLogin ={ this.setIsNotLogin}/>
        // return <Test/>
      }
}

render(<App/>, document.getElementById('app'));